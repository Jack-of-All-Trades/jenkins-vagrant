#!/usr/bin/env python3

from flask import Flask, request, abort

app = Flask(__name__)

@app.route('/', methods=['GET'])
def root_message():
    return "/"

@app.route('/a', methods=['GET'])
def first_route():
    return "a"
