#!/bin/bash

#create dir for jenkins on server. Comment these lines if there is a directory already created for jenkins.
mkdir -p /var/jenkins_home
chown -R 1000:1000 /var/jenkins_home/

#start vagrant
vagrant destroy && vagrant up

#geneate private key
rm ../ci/jenkins/slave/key.pem
openssl rsa -in .vagrant/machines/default/virtualbox/private_key -outform pem > ../ci/jenkins/slave/key.pem

# run jenkins with 3 slave by default. Number of slaves can be changed --scale jenkins-slave=n. 
cd ../ci
chown -R 1000:1000 $PWD/jenkins_home
docker-compose -f docker-compose-ci.yml down && docker-compose -f docker-compose-ci.yml build --no-cache --parallel && docker-compose -f docker-compose-ci.yml up -d --scale jenkins-slave=3