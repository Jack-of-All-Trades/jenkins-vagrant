resource "digitalocean_ssh_key" "primarykey" {
    name = "mykey"
    public_key = "${file("${var.PUBLIC_KEY}")}"
}   
