resource "digitalocean_droplet" "jenkins" {
    image = "${var.IMAGE}"
    name = "jenkinsserver"
    region = "${var.REGION}"
    size = "${var.SIZE[0]}"
    ssh_keys = ["${digitalocean_ssh_key.primarykey.fingerprint}"]
    tags = ["${digitalocean_tag.default.id}"]

    provisioner "file" {
    source = "script.sh"
    destination = "/tmp/script.sh"
  }

    provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/script.sh",
      "sudo /tmp/script.sh"
    ]
  }
  connection {
    user = "${var.INSTANCE_USERNAME}"
    private_key = "${file("${var.PRIVATE_KEY}")}"
    agent = "false"
  }
}