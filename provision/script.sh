#!/bin/bash

# sleep until instance is ready
until [[ -f /var/lib/cloud/instance/boot-finished ]]; do
  sleep 1
done

yum -y update

# install required packages required for virualbox and vagrant
yum -y install nginx python-pip git gcc dkms make qt libgomp patch kernel-headers kernel-devel binutils glibc-headers glibc-devel font-forge epel-release

cd /etc/yum.repo.d/

wget http://download.virtualbox.org/virtualbox/rpm/rhel/virtualbox.repo

#install virtual box version 5.2
yum install -y VirtualBox-5.2

/sbin/rcvboxdrv setup

#install vagrant version 2.2.3 64 bit
yum -y install https://releases.hashicorp.com/vagrant/2.2.3/vagrant_2.2.3_x86_64.rpm


# install docker and docker-compose
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

yum install -y docker-ce

usermod -aG docker $(whoami)

systemctl enable docker.service

systemctl start docker.service

systemctl enable nginx

systemctl start nginx

# pip install docker-compose

# yum upgrade python*